const fs = require("fs");
const path = require("path");

// Function to create a directory of random JSON files
function createRandomJsonFiles(directoryPath, numberOfFiles, callback) {
  return new Promise((resolve, reject) => {
    fs.mkdir(directoryPath, (err) => {
      if (err) {
        reject(err);
      }

      let createdFilesCount = 0;
      const filePaths = [];

      for (let files = 0; files < numberOfFiles; files++) {
        const randomData = { randomKey: Math.random() };
        const filePath = path.join(directoryPath, `file${files}.json`);
        const fileContent = JSON.stringify(randomData);

        fs.writeFile(filePath, fileContent, (err) => {
          if (err) {
            reject(err);
          }

          filePaths.push(filePath);
          createdFilesCount++;

          if (createdFilesCount === numberOfFiles) {
            resolve(filePaths);
          }
        });
      }
    });
  });
}
function deleteFile(filePath) {
  return new Promise((resolve, reject) => {
    fs.unlink(filePath, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve(filePath);
      }
    });
  });
}

function deleteFilesSimultaneously(filePaths) {
  const deleteFiles = filePaths.map(deleteFile);
  return Promise.all(deleteFiles);
}
// Main function to coordinate the entire process
function fsProblem1(directoryPath, numberOfFiles) {
  createRandomJsonFiles(directoryPath, numberOfFiles)
    .then((filePaths) => {
      console.log("Random JSON files created:", filePaths);
      return deleteFilesSimultaneously(filePaths);
    })
    .then((deletedFilePaths) => {
      console.log("Files deleted successfully:", deletedFilePaths);
    })
    .catch((err) => {
      console.error("Error:", err);
    });
}

module.exports = fsProblem1;
