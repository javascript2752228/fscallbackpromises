const fs = require("fs");

function readFileAsync(file) {
  return new Promise((resolve, reject) => {
    fs.readFile(file, "utf-8", (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}

function writeFileAsync(file, data) {
  return new Promise((resolve, reject) => {
    fs.writeFile(file, data, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

function appendFileAsync(file, data) {
  return new Promise((resolve, reject) => {
    fs.appendFile(file, data, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

function unlinkAsync(file) {
  return new Promise((resolve, reject) => {
    fs.unlink(file, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

function fsProblem2(file) {
  let lipsumFileData;
  readFileAsync(file)
    .then((lipsumFileData) => {
      // transforming lipsum data to uppercase
      return writeFileAsync(
        "./upperCase.json",
        JSON.stringify(lipsumFileData.toUpperCase(), null, 2)
      );
    })
    .then(() => {
      return appendFileAsync("./filenames.txt", "upperCase.json");
    })
    .then(() => {
      const sentencesArray = lipsumFileData.toUpperCase().split("\n");
      // here i am putting sentences array in sentences.json
      return writeFileAsync(
        "./sentence.json",
        JSON.stringify(sentencesArray, null, 2)
      );
    })
    .then(() => {
      return appendFileAsync("./filenames.txt", " sentence.json");
    })
    .then(() => {
      const sortedContent = sentencesArray.sort();
      // create a file name sortedContent.json and putting sorted content there
      return writeFileAsync(
        "./sortContent.json",
        JSON.stringify(sortedContent, null, 2)
      );
    })
    .then(() => {
      return appendFileAsync("./filenames.txt", " sortContent.json");
    })
    .then(() => {
      
      return readFileAsync("./filenames.txt", "utf-8");
    })
    .then((filesNameString) => {
      const filesNameArray = filesNameString.trim().split(" ");
      // deleting all the files
      return deleteFile(filesNameArray);
    })
    .catch((err) => {
      console.log(err);
    });
}

function deleteFile(arrayOfFiles, number = 0) {
  if (arrayOfFiles.length > number) {
    // deleting a file using fs.unlink method
    return unlinkAsync(`${arrayOfFiles[number]}`)
      .then(() => {
        console.log(`${arrayOfFiles[number]} file is deleted`);
        
        return deleteFile(arrayOfFiles, number + 1);
      })
      .catch((err) => {
        console.error(err);
      });
  }
}

module.exports = fsProblem2;
